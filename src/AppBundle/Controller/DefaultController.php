<?php

namespace AppBundle\Controller;

use Acme\StoreBundle\Document\Product;
use Redis;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
	/**
	 * @Route("/phpinfo", name="homepage")
	 */
	public function indexAction(Request $request)
	{
		echo phpinfo();
		die;
	}
	
	/**
	 * @Route("/test-email", name="test")
	 */
	public function testAction(\Swift_Mailer $mailer)
	{
		$message = new \Swift_Message('Hello Email');
		$message->setFrom('send@example.com')
			->setTo('recipient@example.com')
			->setBody($this->renderView(// app/Resources/views/Emails/registration.html.twig
				'Emails/registration.html.twig', ['name' => 'Test']), 'text/html');
		$mailer->send($message);
		
		return new Response('Działa');
	}
	
	/**
	 * @Route("/test-mongo", name="")
	 */
	public function testMongoAction()
	{
		$product = new Product();
		$product->setName('A Foo Bar');
		$product->setPrice('19.99');
		
		$dm = $this->get('doctrine_mongodb')->getManager();
		$dm->persist($product);
		$dm->flush();
		
		return new Response('Created product id '.$product->getId());
	}
	
	/**
	 * @Route("/get-mongo", name="")
	 */
	public function showAction()
	{
		$product = $this->get('doctrine_mongodb')
			->getRepository('AcmeStoreBundle:Product')
			->find('595360610ac2290007746f31');
		
		if (!$product) {
			throw $this->createNotFoundException('No product found for id ');
		}
		
		dump($product); die;
	}
	
	/**
	 * @Route("/redis", name="")
	 */
	public function redisAction()
	{
		$redis = new Redis();
		$redis->connect('redis', 6379);
		
		$cacheDriver = new \Doctrine\Common\Cache\RedisCache();
		$cacheDriver->setRedis($redis);
		
		dump($cacheDriver->save('cache_id', 'my_data')); die;
	}
	
	/**
	 * @Route("/get-redis", name="")
	 */
	public function getRedisAction()
	{
		$redis = new Redis();
		$redis->connect('redis', 6379);
		
		$cacheDriver = new \Doctrine\Common\Cache\RedisCache();
		$cacheDriver->setRedis($redis);
		
		dump($cacheDriver->fetch('cache_id'));
		die;
	}
}
